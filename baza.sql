CREATE SEQUENCE person_id_sequence;
SELECT setval('person_id_sequence', 1);

CREATE TABLE person
(ID         INT PRIMARY KEY NOT NULL DEFAULT nextval('person_id_sequence'),
LOGIN       TEXT NOT NULL UNIQUE,
PASSWORD    TEXT NOT NULL,
IS_ADMIN    BOOLEAN DEFAULT FALSE NOT NULL);

ALTER SEQUENCE person_id_sequence OWNED BY person.id;


CREATE SEQUENCE event_id_sequence;
SELECT setval('event_id_sequence', 1);

CREATE TABLE event
(ID        INT PRIMARY KEY NOT NULL DEFAULT nextval('event_id_sequence'),
NAME       TEXT NOT NULL UNIQUE,
START      TIMESTAMP WITH TIME ZONE,
FINISH     TIMESTAMP WITH TIME ZONE);

ALTER SEQUENCE event_id_sequence OWNED BY event.id;

CREATE TABLE person_has_event
(PERSON_ID   INT,
EVENT_ID     INT,
FOREIGN KEY (PERSON_ID) REFERENCES person(ID),
FOREIGN KEY (EVENT_ID) REFERENCES event(ID));


CREATE SEQUENCE talk_id_sequence;
SELECT setval('talk_id_sequence', 1);

-- propozycja referatu jako room_id ma null, natomiast zatwierdzony referat konkretną liczbę
CREATE TABLE talk
(ID                INT PRIMARY KEY NOT NULL DEFAULT nextval('talk_id_sequence'),
IDENTIFIER         TEXT NOT NULL UNIQUE,
TITLE              TEXT NOT NULL,
START              TIMESTAMP WITH TIME ZONE NOT NULL,
ROOM_ID            TEXT,
CONFIRMATION_DATE  TIMESTAMP WITH TIME ZONE);

ALTER SEQUENCE event_id_sequence OWNED BY event.id;

CREATE OR REPLACE FUNCTION set_confirmation_date()
   RETURNS TRIGGER AS $X$
   BEGIN
      IF NEW.ROOM_ID IS NOT NULL
      THEN
        SELECT now() INTO NEW.CONFIRMATION_DATE;
      END IF;

      RETURN NEW;
   END
$X$ LANGUAGE plpgsql;

CREATE TRIGGER set_confirmation_date_trigger_before_insert BEFORE INSERT ON talk
FOR EACH ROW EXECUTE PROCEDURE set_confirmation_date();

CREATE TRIGGER set_confirmation_date_trigger_before_update BEFORE UPDATE ON talk
FOR EACH ROW EXECUTE PROCEDURE set_confirmation_date();

CREATE TABLE rejected_talk
(IDENTIFIER     TEXT NOT NULL,
TITLE           TEXT NOT NULL,
START           TIMESTAMP WITH TIME ZONE,
SPEAKER_ID      INT,
FOREIGN KEY (SPEAKER_ID) REFERENCES person(ID));


CREATE TABLE person_has_talk
(PERSON_ID  INT,
TALK_ID     INT,
FOREIGN KEY (PERSON_ID) REFERENCES person(ID),
FOREIGN KEY (TALK_ID) REFERENCES talk(ID) ON DELETE CASCADE);


CREATE TABLE talk_presented_on_event
(TALK_ID    INT,
EVENT_ID    INT,
FOREIGN KEY (TALK_ID) REFERENCES talk(ID),
FOREIGN KEY (EVENT_ID) REFERENCES event(ID));


CREATE TABLE talk_rating
(TALK_ID        INT,
GRANTED_BY_ID   INT,
RATING          INT  NOT NULL  CHECK(RATING >= 0 AND RATING <=10),
AUTHOR_ATTENDED BOOLEAN NOT NULL,
FOREIGN KEY (TALK_ID) REFERENCES talk(ID),
FOREIGN KEY (GRANTED_BY_ID) REFERENCES person(ID));


CREATE TABLE person_attended_talk
(PERSON_ID         INT,
TALK_ID            INT,
--CONFIRMED_ATTEND   BOOLEAN  DEFAULT FALSE NOT NULL,
FOREIGN KEY (PERSON_ID) REFERENCES person(ID),
FOREIGN KEY (TALK_ID) REFERENCES TALK(ID),
UNIQUE (PERSON_ID, TALK_ID));


CREATE OR REPLACE FUNCTION update_talk_rating_context()
   RETURNS TRIGGER AS $X$
   BEGIN
      UPDATE talk_rating
      SET AUTHOR_ATTENDED = true
      WHERE GRANTED_BY_ID = NEW.PERSON_ID;

      RETURN NEW;
   END
$X$ LANGUAGE plpgsql;

CREATE TRIGGER update_talk_rating_context_trigger AFTER INSERT ON person_attended_talk
FOR EACH ROW EXECUTE PROCEDURE update_talk_rating_context();


CREATE TABLE person_is_friend_of
(PERSON_ID1    INT,
PERSON_ID2     INT,
FOREIGN KEY (PERSON_ID1) REFERENCES person(ID),
FOREIGN KEY (PERSON_ID2) REFERENCES person(ID),
CHECK (PERSON_ID1 != PERSON_ID2));



CREATE TABLE friendship_proposal
(PROPOSED_BY_ID   INT,
PROPOSED_FOR_ID   INT,
FOREIGN KEY (PROPOSED_BY_ID) REFERENCES person(ID),
FOREIGN KEY (PROPOSED_FOR_ID) REFERENCES person(ID),
UNIQUE (PROPOSED_BY_ID, PROPOSED_FOR_ID),
CHECK (PROPOSED_BY_ID != PROPOSED_FOR_ID));


CREATE OR REPLACE FUNCTION two_proposals_equal_friendship()
   RETURNS TRIGGER AS $X$
   BEGIN
      IF EXISTS (
        SELECT * FROM friendship_proposal
        WHERE PROPOSED_BY_ID = NEW.PROPOSED_FOR_ID
        AND   PROPOSED_FOR_ID = NEW.PROPOSED_BY_ID)
      THEN
        INSERT INTO person_is_friend_of
        VALUES (NEW.PROPOSED_BY_ID, NEW.PROPOSED_FOR_ID);

        DELETE FROM friendship_proposal
        WHERE (PROPOSED_BY_ID = NEW.PROPOSED_FOR_ID
          AND PROPOSED_FOR_ID = NEW.PROPOSED_BY_ID)
          OR  (PROPOSED_BY_ID = NEW.PROPOSED_BY_ID
          AND PROPOSED_FOR_ID = NEW.PROPOSED_FOR_ID);
      END IF;
      RETURN NEW;
   END
$X$ LANGUAGE plpgsql;

-- zawarcie znajomości następuje automatycznie po zarejestrowaniu dwóch symetrycznych propozycji
CREATE TRIGGER add_friend AFTER INSERT ON friendship_proposal
FOR EACH ROW EXECUTE PROCEDURE two_proposals_equal_friendship();
