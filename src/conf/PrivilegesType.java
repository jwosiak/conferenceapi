package conf;

/**
 * Created by jakub on 02.06.17.
 */
public enum PrivilegesType {
    STANDARD,
    ADMIN,
    NONE
}
