package conf;

/**
 * Created by jakub on 21.05.17.
 */
public enum StatusCode {
    OK,
    ERROR,
    NOT_IMPLEMENTED
}
