package conf;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;

/**
 * Created by jakub on 19.05.17.
 */
public class SqlDataProvider {
    PsqlConnector connector;
    Statement currentStatement;

    public void setConnector(PsqlConnector connector){
        this.connector = connector;
    }

    public ResultSet executeQuery(String sqlString) throws SQLException {
        if (currentStatement != null){
            currentStatement.close();
        }

        currentStatement = connector.getConnection().createStatement();

        ResultSet resultSet = currentStatement.executeQuery(sqlString);

        return resultSet;
    }

    public void executeUpdate(String sqlString) throws SQLException {
        Statement stmt = connector.getConnection().createStatement();

        stmt.executeUpdate(sqlString);

        stmt.close();
    }


    public void initDatabase(String sqlFilename) throws SQLException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(sqlFilename));
        try {
            StringBuilder stringBuilder = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line = br.readLine();
            }

            executeQuery(stringBuilder.toString());
        } finally {
            br.close();
        }
    }


    public String getCellValue(String tableName, String returningColumn, String compColumn, String compColumnValue,
                            boolean compColumnIsString) throws SQLException {
        String sql;
        if (compColumnIsString) {
            sql = String.format("SELECT %s FROM %s WHERE %s = '%s';", returningColumn, tableName, compColumn,
                    compColumnValue);
        }
        else{
            sql = String.format("SELECT %s FROM %s WHERE %s = %s;", returningColumn,
                    tableName, compColumn, compColumnValue);
        }

        ResultSet rs = executeQuery(sql);
        rs.next();

        return rs.getString(returningColumn);
    }

    public boolean rowWithValueExists(String tableName, String compColumn, String compColumnValue,
                                      boolean compColumnIsString) throws SQLException {
        String sql;
        if (compColumnIsString) {
            sql = String.format("SELECT * FROM %s WHERE %s = '%s';", tableName, compColumn, compColumnValue);
        }
        else{
            sql = String.format("SELECT * FROM %s WHERE %s = %s;", tableName, compColumn, compColumnValue);
        }

        ResultSet rs = executeQuery(sql);
        return rs.next();
    }

    public boolean rowWithConditionExists(String tableName, String whereCondition) throws SQLException {
        String sql = String.format("SELECT * FROM %s %s", tableName, whereCondition);

        ResultSet rs = executeQuery(sql);
        return rs.next();
    }

    public boolean isTimestampBetween(String timestamp, String lValue, String rValue) throws SQLException {
        StringBuilder sql = new StringBuilder();

        sql
                .append(String.format("SELECT * FROM (SELECT CAST('%s' AS TIMESTAMP WITH TIME ZONE) AS \"d\") q ",
                        timestamp))
                .append(String.format("WHERE \"d\" >= CAST('%s' AS TIMESTAMP WITH TIME ZONE) ", lValue))
                .append(String.format("AND \"d\" <= CAST('%s' AS TIMESTAMP WITH TIME ZONE) ", rValue));

        ResultSet rs = executeQuery(sql.toString());
        return rs.next();
    }
}
