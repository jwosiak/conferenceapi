package conf;

import java.sql.ResultSet;

/**
 * Created by jakub on 21.05.17.
 */
public class UserAuthorizer {
    static public PrivilegesType authorize(String login, String password, SqlDataProvider dataProvider) throws Exception {
        StringBuilder queryBuilder = new StringBuilder();

        queryBuilder
                .append("SELECT IS_ADMIN FROM person ")
                .append(String.format("WHERE LOGIN = '%s' AND PASSWORD = '%s';", login, password));


        ResultSet resultSet = dataProvider.executeQuery(queryBuilder.toString());

        if (!resultSet.next()){
            return PrivilegesType.NONE;
        }


        if (resultSet.getBoolean("IS_ADMIN")) {
            return PrivilegesType.ADMIN;
        }

        return PrivilegesType.STANDARD;
    }
}
