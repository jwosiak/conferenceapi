package conf.io;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by jakub on 04.06.17.
 */
public class FunctionsReader {
    InputStream stream;
    Scanner scanner;

    String funName;
    Map<String, String> parameters;

    public FunctionsReader(InputStream stream){
        this.stream = stream;
        scanner = new Scanner(stream);
    }

    public boolean next() {
        if (!scanner.hasNext()){
            return false;
        }

        String line = scanner.nextLine();
        line = line.replaceAll("'", " ");
        JSONObject signature = new JSONObject( line );

        funName = (String) signature.names().get(0);

        if (funName == null){
            parameters = null;
            return false;
        }

        this.parameters = new HashMap<String, String>();

        JSONObject parameters = (JSONObject) signature.get(funName);

        for (Object key : parameters.names()){
            String skey = (String) key;
            this.parameters.put(skey, parameters.get(skey).toString() );
        }
        return true;
    }

    public String getFunName(){
        return funName;
    }

    public Map<String, String> getParameters(){
        return parameters;
    }
}
