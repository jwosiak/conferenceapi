package conf.io;

import conf.StatusCode;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by jakub on 06.06.17.
 */
public class FunctionsOutputWriter {

    String statusCodeToString(StatusCode statusCode){
        switch (statusCode){
            case OK:
                return "OK";
            case ERROR:
                return "ERROR";
            default:
                return "NOT IMPLEMENTED";
        }
    }

    public String output(StatusCode statusCode, List<HashMap<String, String>> results){
        StringBuilder output = new StringBuilder();

        output.append(String.format("{ \"status\": \"%s\"", statusCodeToString(statusCode) ));

        if (results != null) {
            output.append(", \"data\": [ ");

            for (HashMap<String, String> res : results) {
                output.append("{");
                for (String key : res.keySet()){
                    output.append(String.format(" \"%s\": \"%s\",", key, res.get(key)));
                }
                output.deleteCharAt(output.length()-1);
                output.append(" }, ");
            }
            if (!results.isEmpty()){
                output.deleteCharAt(output.length() - 1);
            }
            output.deleteCharAt(output.length() - 1);

            output.append(" ]");
        }

        output.append(" }");

        return output.toString();
    }
}
