package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by jakub on 03.06.17.
 */
public class TalkProcedure extends ConfProcedure {
    class TalkDateNotInsideEventDate extends Exception{

    }

    public TalkProcedure(Map<String, String> parameters){
        super(parameters);
        requiredPrivileges = PrivilegesType.ADMIN;
    }

    void associateTalkWithEvent(String talkIntId, SqlDataProvider dataProvider) throws SQLException {
        if (!param("eventname").isEmpty()){
            int eventId = Integer.parseInt(
                    dataProvider.getCellValue("event", "ID", "NAME", param("eventname"), true));

            String sqlTalkPresentedOnEvent = String.format("INSERT INTO talk_presented_on_event VALUES (%s, %s)",
                    talkIntId, eventId);

            dataProvider.executeUpdate(sqlTalkPresentedOnEvent);
        }
    }

    boolean checkDateCorrectness(SqlDataProvider dataProvider) throws SQLException {
        String eventStart = dataProvider.getCellValue("event", "START", "NAME",
                param("eventname"), true);

        String eventFinish = dataProvider.getCellValue("event", "FINISH", "NAME",
                param("eventname"), true);

        return dataProvider.isTimestampBetween(param("start_timestamp"), eventStart, eventFinish);
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        if (!param("eventname").isEmpty() && !checkDateCorrectness(dataProvider)){
            throw new TalkDateNotInsideEventDate();
        }

        String adminId = dataProvider.getCellValue(
                "person", "ID", "LOGIN", param("login"), true);

        String speakerId =  dataProvider.getCellValue(
                "person", "ID", "LOGIN", param("speakerlogin"), true);

        String sqlPersonHasTalk;
        String sqlTalk;

        String sqlEvaluation;

        String talkIntId;

        boolean talkExistsAndIsUnConfirmed =
                dataProvider.rowWithConditionExists("talk",
                        String.format("WHERE ROOM_ID IS NULL AND IDENTIFIER = '%s' ", param("talk")));

        if (talkExistsAndIsUnConfirmed){
            talkIntId = dataProvider.getCellValue("talk", "ID", "IDENTIFIER",
                    param("talk"), true);

            sqlPersonHasTalk = String.format("UPDATE person_has_talk SET PERSON_ID = %s WHERE TALK_ID = %s",
                     speakerId, talkIntId);

            sqlTalk = String.format(
                    "UPDATE talk SET TITLE = '%s', START = CAST('%s' AS TIMESTAMP WITH TIME ZONE), ROOM_ID = '%s' WHERE ID = %s",
                    param("title"), param("start_timestamp"), param("room"), talkIntId);

            dataProvider.executeUpdate(sqlTalk);
        }
        else{
            sqlTalk = String.format("INSERT INTO talk (IDENTIFIER,TITLE,START,ROOM_ID) VALUES " +
                            "('%s', '%s', CAST('%s' AS TIMESTAMP WITH TIME ZONE), '%s')",
                    param("talk"), param("title"), param("start_timestamp"), param("room"));

            dataProvider.executeUpdate(sqlTalk);

            talkIntId = dataProvider.getCellValue("talk", "ID", "IDENTIFIER",
                    param("talk"), true);

            sqlPersonHasTalk =
                    String.format("INSERT INTO person_has_talk VALUES (%s, %s)", speakerId, talkIntId);
        }

        sqlEvaluation =
                String.format("INSERT INTO talk_rating VALUES (%s, %s, %s, true)", talkIntId, adminId,
                        param("initial_evaluation"));


        associateTalkWithEvent(talkIntId, dataProvider);

        dataProvider.executeUpdate(sqlPersonHasTalk);
        dataProvider.executeUpdate(sqlEvaluation);
    }
}
