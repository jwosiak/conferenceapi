package conf.functions;

import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 07.06.17.
 */
public class DayPlanFunction extends ConfFunction {
    public DayPlanFunction(Map<String, String> parameters) {
        super(parameters);
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();

        sql
                .append(String.format("SELECT * FROM talk WHERE CAST(START AS DATE) = CAST('%s' AS DATE) ",
                        param("timestamp")))
                .append("AND ROOM_ID IS NOT NULL ORDER BY ROOM_ID, START ");

        result = dataProvider.executeQuery(sql.toString());
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("room", result.getString("ROOM_ID"));

        return tuple;
    }
}
