package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 08.06.17.
 */
public class FriendsEventsFunction extends ConfFunction {
    public FriendsEventsFunction(Map<String, String> parameters) {
        super(parameters);
        requiredPrivileges = PrivilegesType.STANDARD;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();

        String userID =
                dataProvider.getCellValue("person", "ID", "LOGIN", param("login"), true);

        sql
                .append("SELECT * FROM person p JOIN person_has_event ON (p.ID = PERSON_ID) ")
                .append("JOIN event t ON (EVENT_ID = t.ID) ")
                .append(String.format("WHERE NAME = '%s' AND EXISTS ", param("eventname")))
                .append(String.format(
                        "(SELECT * FROM person_is_friend_of WHERE %s IN (PERSON_ID1, PERSON_ID2) ", userID))
                .append("AND p.ID IN (PERSON_ID1, PERSON_ID2)) ");

        result = dataProvider.executeQuery(sql.toString());
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("eventname", result.getString("NAME"));
        tuple.put("login", param("login"));
        tuple.put("friendlogin", result.getString("LOGIN"));

        return tuple;
    }
}
