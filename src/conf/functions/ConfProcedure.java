package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;
import conf.StatusCode;
import conf.UserAuthorizer;

import java.util.Map;

/**
 * Created by jakub on 21.05.17.
 */
public abstract class ConfProcedure {

    protected PrivilegesType requiredPrivileges;

    protected Map<String, String> parameters;

    protected abstract void procedureLogic(SqlDataProvider dataProvider) throws Exception;

    public ConfProcedure(Map<String, String> parameters){
        requiredPrivileges = PrivilegesType.NONE;
        this.parameters = parameters;
    }

    protected String param(String key){
        String value = parameters.get(key);
        if (value == null){
            throw new NullPointerException();
        }
        return value;
    }

    boolean hasEnoughPrivileges(PrivilegesType actual, PrivilegesType required){
        switch (required){
            default:
            case NONE:
                return true;
            case STANDARD:
                return actual != PrivilegesType.NONE;
            case ADMIN:
                return actual == PrivilegesType.ADMIN;
        }
    }

    public StatusCode invoke(SqlDataProvider dataProvider){
        try{
            PrivilegesType userPrivileges = requiredPrivileges == PrivilegesType.NONE? PrivilegesType.NONE :
                    UserAuthorizer.authorize(param("login"), param("password"), dataProvider);
            if (hasEnoughPrivileges(userPrivileges, requiredPrivileges)){
                procedureLogic(dataProvider);
                return StatusCode.OK;
            }
        } catch (Exception e){
        }
        return StatusCode.ERROR;
    }
}
