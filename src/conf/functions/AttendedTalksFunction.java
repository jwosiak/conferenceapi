package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 07.06.17.
 */
public class AttendedTalksFunction extends ConfFunction {
    public AttendedTalksFunction(Map<String, String> parameters) {
        super(parameters);
        requiredPrivileges = PrivilegesType.STANDARD;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();

        sql
                .append("SELECT * FROM person p JOIN person_attended_talk pat ON (p.ID = pat.PERSON_ID) ")
                .append("JOIN talk t ON (t.ID = pat.TALK_ID) ")
                .append(String.format("WHERE p.LOGIN = '%s' ", param("login")));

        result = dataProvider.executeQuery(sql.toString());
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("room", result.getString("ROOM_ID"));

        return tuple;
    }
}
