package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.util.Map;

/**
 * Created by jakub on 04.06.17.
 */
public class EvaluationProcedure extends ConfProcedure {
    class TalkIsNotConfirmedException extends Exception{

    }

    public EvaluationProcedure(Map<String, String> parameters){
        super(parameters);
        requiredPrivileges = PrivilegesType.STANDARD;
    }

    void throwIfTalkIsNotConfirmed(String talkIntId, SqlDataProvider dataProvider) throws Exception {
        boolean talkIsConfirmed = dataProvider.rowWithConditionExists("talk",
                String.format("WHERE ID = %s AND ROOM_ID IS NOT NULL", talkIntId) );

        if (!talkIsConfirmed){
            throw new TalkIsNotConfirmedException();
        }
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        String talkIntId = dataProvider
                .getCellValue("talk", "ID", "IDENTIFIER", param("talk"), true);

        throwIfTalkIsNotConfirmed(talkIntId, dataProvider);

        String personId = dataProvider.getCellValue("person", "ID", "LOGIN",
                param("login"), true);

        boolean attendedTalk = dataProvider.rowWithConditionExists(
                "person_attended_talk",
                String.format("WHERE PERSON_ID = %s AND TALK_ID = %s", personId, talkIntId) );


        String sql;

        if (attendedTalk){
            sql = String.format("INSERT INTO talk_rating VALUES (%s, %s, %s, true)",
                    talkIntId, personId, param("rating"));
        }
        else{
            sql = String.format("INSERT INTO talk_rating VALUES (%s, %s, %s, false)",
                    talkIntId, personId, param("rating"));
        }

        dataProvider.executeUpdate(sql);
    }
}
