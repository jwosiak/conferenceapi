package conf.functions;

import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 07.06.17.
 */
public class MostPopularTalksFunction extends ConfFunction {
    public MostPopularTalksFunction(Map<String, String> parameters) {
        super(parameters);
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT ID, IDENTIFIER, TITLE, START, ROOM_ID, COUNT(pat.PERSON_ID) FROM ")
                .append("talk t JOIN person_attended_talk pat ON (t.ID = pat.TALK_ID) WHERE ")
                .append(String.format(
                        "(t.START BETWEEN CAST('%s' AS TIMESTAMP WITH TIME ZONE) ", param("start_timestamp")))
                .append(String.format("AND CAST('%s' AS TIMESTAMP WITH TIME ZONE)) ", param("end_timestamp")))
                .append("AND ROOM_ID IS NOT NULL ")
                .append("GROUP BY (ID, IDENTIFIER, TITLE, START, ROOM_ID)")
                .append("ORDER BY COUNT(pat.PERSON_ID) DESC ")
                .append(limit());

        result = dataProvider.executeQuery(sql.toString());
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("room", result.getString("ROOM_ID"));

        return tuple;
    }
}
