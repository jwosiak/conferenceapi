package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 08.06.17.
 */
public class FriendsTalksFunction extends ConfFunction {
    public FriendsTalksFunction(Map<String, String> parameters) {
        super(parameters);
        requiredPrivileges = PrivilegesType.STANDARD;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();

        String userID =
                dataProvider.getCellValue("person", "ID", "LOGIN", param("login"), true);

        sql
                .append("SELECT * FROM person p JOIN person_has_talk ON (p.ID = PERSON_ID) ")
                .append("JOIN talk t ON (TALK_ID = t.ID) ")
                .append("WHERE ROOM_ID IS NOT NULL AND EXISTS ")
                .append(String.format(
                        "(SELECT * FROM person_is_friend_of WHERE %s IN (PERSON_ID1, PERSON_ID2) ", userID))
                .append("AND p.ID IN (PERSON_ID1, PERSON_ID2)) ")
                .append(String.format(
                        "AND START BETWEEN CAST('%s' AS TIMESTAMP WITH TIME ZONE) ", param("start_timestamp")))
                .append(String.format("AND CAST('%s' AS TIMESTAMP WITH TIME ZONE) ", param("end_timestamp")))
                .append("ORDER BY START ")
                .append(limit());

        result = dataProvider.executeQuery(sql.toString());
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("speakerlogin", result.getString("LOGIN"));
        tuple.put("room", result.getString("ROOM_ID"));

        return tuple;
    }
}
