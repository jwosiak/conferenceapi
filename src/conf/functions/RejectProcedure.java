package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.util.Map;

/**
 * Created by jakub on 03.06.17.
 */
public class RejectProcedure extends ConfProcedure {
    class TalkCantBeConfirmedException extends Exception {

    }

    public RejectProcedure(Map<String, String> parameters){
        super(parameters);
        requiredPrivileges = PrivilegesType.ADMIN;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        boolean talkNotConfirmedYet =
                dataProvider.rowWithConditionExists("talk",
                        String.format("WHERE ROOM_ID IS NULL AND IDENTIFIER = '%s' ", param("talk")));

        if (!talkNotConfirmedYet){
            throw new TalkCantBeConfirmedException();
        }

        StringBuilder sqlInsertRejected = new StringBuilder();

        sqlInsertRejected
                .append("INSERT INTO rejected_talk SELECT IDENTIFIER, TITLE, START, PERSON_ID ")
                .append("FROM talk t JOIN person_has_talk pht ON (t.ID = pht.TALK_ID) ")
                .append(String.format("WHERE IDENTIFIER = '%s' ", param("talk")));

        String sqlDeleteTalk = String.format(
                "DELETE FROM talk WHERE IDENTIFIER = '%s'", param("talk"));


        dataProvider.executeUpdate(sqlInsertRejected.toString());
        dataProvider.executeUpdate(sqlDeleteTalk);
    }
}
