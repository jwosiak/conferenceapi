package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;
import conf.UserAuthorizer;

import java.util.Map;

/**
 * Created by jakub on 02.06.17.
 */
public class EventProcedure extends ConfProcedure {
    public EventProcedure(Map<String, String> parameters){
        super(parameters);
        requiredPrivileges = PrivilegesType.ADMIN;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql
                .append("INSERT INTO event (NAME,START,FINISH) VALUES ")
                .append(String.format(
                        "('%s', CAST('%s' AS TIMESTAMP WITH TIME ZONE), CAST('%s' AS TIMESTAMP WITH TIME ZONE))",
                        param("eventname"), param("start_timestamp"), param("end_timestamp")));

        dataProvider.executeUpdate(sql.toString());
    }
}
