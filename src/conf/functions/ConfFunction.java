package conf.functions;

import conf.StatusCode;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by jakub on 06.06.17.
 */
public abstract class ConfFunction extends ConfProcedure {
    public ConfFunction(Map<String, String> parameters) {
        super(parameters);
    }

    protected ResultSet result;

    protected abstract HashMap<String, String> resultSetRowToTuple() throws SQLException;

    protected String limit(){
        int limit = Integer.parseInt(param("limit"));
        if (limit != 0){
            return String.format("LIMIT %s ", limit);
        }
        return "";
    }

    public LinkedList<HashMap<String, String>> results() throws Exception {
        LinkedList<HashMap<String, String>> resultTuples = new LinkedList<>();

        while (result.next()){
            resultTuples.add(resultSetRowToTuple());
        }
        return resultTuples;
    }
}
