package conf.functions;

import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 06.06.17.
 */
public class UserPlanFunction extends ConfFunction {
    public UserPlanFunction(Map<String, String> parameters) {
        super(parameters);
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("room", result.getString("ROOM_ID"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("login", result.getString("LOGIN"));
//        tuple.put("login", param("login"));

        return tuple;
    }


    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        String userId = dataProvider.getCellValue("person", "ID", "LOGIN", param("login"),true);

        StringBuilder sql = new StringBuilder();

        sql
                .append("SELECT * FROM talk t ")
                .append("JOIN talk_presented_on_event tpoe ON(t.ID = tpoe.TALK_ID) ")
                .append("JOIN person_has_event phe ON(phe.EVENT_ID = tpoe.EVENT_ID) ")
                .append("JOIN person_has_talk pht ON (t.ID = pht.TALK_ID) ")
                .append("JOIN person p on (p.ID = pht.PERSON_ID) ")
                .append(String.format("WHERE phe.PERSON_ID = %s AND t.ROOM_ID IS NOT NULL ", userId))
                .append("AND START >= now() ORDER BY t.START ")
                .append(limit());

        result = dataProvider.executeQuery(sql.toString());
    }
}
