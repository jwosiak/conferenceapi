package conf.functions;

import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 07.06.17.
 */
public class BestTalksFunction extends ConfFunction {
    public BestTalksFunction(Map<String, String> parameters) {
        super(parameters);
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();

        int all = Integer.parseInt(param("all"));

        sql
                .append("SELECT IDENTIFIER, TITLE, START, ROOM_ID ")
                .append("FROM talk t JOIN talk_rating tr ON (t.ID = tr.TALK_ID) WHERE (t.START BETWEEN ")
                .append(String.format(
                        "CAST('%s' AS TIMESTAMP WITH TIME ZONE) AND CAST('%s' AS TIMESTAMP WITH TIME ZONE)) ",
                        param("start_timestamp"), param("end_timestamp")));

        if (all != 1){
            sql.append("AND tr.AUTHOR_ATTENDED ");
        }

        sql.append("GROUP BY IDENTIFIER, TITLE, START, ROOM_ID ORDER BY AVG(RATING) DESC ");
        sql.append(limit());

        result = dataProvider.executeQuery(sql.toString());
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("room", result.getString("ROOM_ID"));

        return tuple;
    }
}
