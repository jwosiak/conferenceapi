package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.util.Map;

/**
 * Created by jakub on 03.06.17.
 */
public class RegisterForEventProcedure extends ConfProcedure {
    public RegisterForEventProcedure(Map<String, String> parameters){
        super(parameters);
        requiredPrivileges = PrivilegesType.STANDARD;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        String personId = dataProvider.getCellValue("person", "ID", "LOGIN",
                                                    param("login"), true);
        String eventId = dataProvider.getCellValue("event", "ID", "NAME",
                                                   param("eventname"), true);

        String sql = 
            String.format("INSERT INTO person_has_event VALUES (%s,%s)",
                          personId, eventId);

        dataProvider.executeUpdate(sql);
    }
}
