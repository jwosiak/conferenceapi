package conf.functions;

import conf.SqlDataProvider;
import conf.StatusCode;

import java.util.Map;

/**
 * Created by jakub on 02.06.17.
 */
public class OrganizerProcedure extends ConfProcedure {
    public OrganizerProcedure(Map<String, String> parameters) {
        super(parameters);
    }

    class WrongSecretKeyException extends Exception{

    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        if (param("secret").compareTo("d8578edf8458ce06fbc5bb76a58c5ca4") != 0 ){
            throw new WrongSecretKeyException();
        }

        String sql = String.format("INSERT INTO person (LOGIN,PASSWORD,IS_ADMIN) VALUES ('%s','%s',true);",
                param("newlogin"), param("newpassword"));

        dataProvider.executeUpdate(sql);
    }
}
