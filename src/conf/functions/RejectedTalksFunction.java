package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;
import conf.StatusCode;
import conf.UserAuthorizer;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 08.06.17.
 */
public class RejectedTalksFunction extends ConfFunction {
    public RejectedTalksFunction(Map<String, String> parameters) {
        super(parameters);
        requiredPrivileges = PrivilegesType.STANDARD;
    }

    PrivilegesType userPrivileges;

    @Override
    public StatusCode invoke(SqlDataProvider dataProvider){
        try{
            PrivilegesType userPrivileges = requiredPrivileges == PrivilegesType.NONE? PrivilegesType.NONE :
                    UserAuthorizer.authorize(param("login"), param("password"), dataProvider);
            if (hasEnoughPrivileges(userPrivileges, requiredPrivileges)){
                this.userPrivileges = userPrivileges;
                procedureLogic(dataProvider);
                return StatusCode.OK;
            }
        } catch (Exception e){
        }
        return StatusCode.ERROR;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT * FROM rejected_talk JOIN person ON (ID = SPEAKER_ID) ");

        if (userPrivileges == PrivilegesType.STANDARD){
            sql.append(String.format("WHERE LOGIN = '%s' ", param("login")));
        }

        result = dataProvider.executeQuery(sql.toString());
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("speakerlogin", result.getString("LOGIN"));

        return tuple;
    }
}
