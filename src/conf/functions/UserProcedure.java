package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.util.Map;

/**
 * Created by jakub on 03.06.17.
 */
public class UserProcedure extends ConfProcedure {
    public UserProcedure(Map<String, String> parameters){
        super(parameters);
        requiredPrivileges = PrivilegesType.ADMIN;
    }
    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        String sql = String.format(
                "INSERT INTO person (LOGIN, PASSWORD) values ('%s','%s')", param("newlogin"), param("newpassword"));

        dataProvider.executeUpdate(sql);
    }
}
