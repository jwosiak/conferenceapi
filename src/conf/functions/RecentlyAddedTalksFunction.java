package conf.functions;

import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 08.06.17.
 */
public class RecentlyAddedTalksFunction extends ConfFunction {
    public RecentlyAddedTalksFunction(Map<String, String> parameters) {
        super(parameters);
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        String sql = String.format(
                "SELECT * FROM talk WHERE ROOM_ID IS NOT NULL ORDER BY CONFIRMATION_DATE DESC %s ", limit());
        result = dataProvider.executeQuery(sql);
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("room", result.getString("ROOM_ID"));

        return tuple;
    }
}
