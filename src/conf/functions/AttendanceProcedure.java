package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.util.Map;

/**
 * Created by jakub on 04.06.17.
 */
public class AttendanceProcedure extends ConfProcedure {
    class TalkIsNotConfirmedException extends Exception {

    }

    public AttendanceProcedure(Map<String, String> parameters) {
        super(parameters);
        requiredPrivileges = PrivilegesType.STANDARD;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        boolean talkIsConfirmed = dataProvider.rowWithConditionExists(
                "talk", String.format("WHERE IDENTIFIER = '%s' AND ROOM_ID IS NOT NULL", param("talk")));

        if (!talkIsConfirmed){
            throw new TalkIsNotConfirmedException();
        }

        String personId = dataProvider.getCellValue("person", "ID", "LOGIN",
                param("login"), true);

        String talkIntId = dataProvider
                .getCellValue("talk", "ID", "IDENTIFIER", param("talk"), true);

        String sql = String.format("INSERT INTO person_attended_talk VALUES (%s, %s)", personId, talkIntId);

        dataProvider.executeUpdate(sql);
    }
}
