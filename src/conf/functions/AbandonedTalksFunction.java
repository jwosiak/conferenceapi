package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 07.06.17.
 */
public class AbandonedTalksFunction extends ConfFunction {
    public AbandonedTalksFunction(Map<String, String> parameters) {
        super(parameters);
        requiredPrivileges = PrivilegesType.ADMIN;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();

        sql
                .append("SELECT IDENTIFIER, TITLE, START, ROOM_ID, COUNT(*) AS \"NUMBER\" ")
                .append("FROM talk t JOIN talk_presented_on_event tpoe ON (t.ID = tpoe.TALK_ID) ")
                .append("JOIN person_has_event phe ON (tpoe.EVENT_ID = phe.EVENT_ID) ")
                .append("WHERE NOT EXISTS (SELECT * FROM person_attended_talk pat WHERE pat.TALK_ID = tpoe.TALK_ID ")
                .append("AND pat.PERSON_ID = phe.PERSON_ID) ")
                .append("GROUP BY (IDENTIFIER, TITLE, START, ROOM_ID) ")
                .append("ORDER BY COUNT(*) DESC ")
                .append(limit());

        result = dataProvider.executeQuery(sql.toString());
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("room", result.getString("ROOM_ID"));
        tuple.put("number", result.getString("NUMBER"));

        return tuple;
    }
}
