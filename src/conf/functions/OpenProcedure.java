package conf.functions;

import conf.PsqlConnector;
import conf.SqlDataProvider;

import javax.security.auth.login.FailedLoginException;
import java.sql.ResultSet;
import java.util.Map;

/**
 * Created by jakub on 21.05.17.
 */
public class OpenProcedure extends ConfProcedure {
    class FailedToInitDataBaseException extends Exception {

    }

    public OpenProcedure(Map<String, String> parameters) {
        super(parameters);
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        PsqlConnector connector = new PsqlConnector(param("baza"), param("login"), param("password"));

        connector.connect();

        dataProvider.setConnector(connector);

        String sql = String.format("SELECT * FROM pg_tables WHERE TABLEOWNER = '%s';", param("login"));
        ResultSet rs = dataProvider.executeQuery(sql);

        if (!rs.next()){
            try{
                dataProvider.initDatabase("baza.sql");
            }
            catch (Exception e){
                rs = dataProvider.executeQuery(sql);
                if (!rs.next()){
                    throw new FailedToInitDataBaseException();
                }
            }
        }
    }
}
