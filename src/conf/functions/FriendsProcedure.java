package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;
import conf.StatusCode;
import conf.UserAuthorizer;

import java.sql.ResultSet;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by jakub on 03.06.17.
 */
public class FriendsProcedure extends ConfProcedure {
    class PersonsAreFriendsException extends Exception {

    }

    public FriendsProcedure(Map<String, String> parameters){
        super(parameters);
        requiredPrivileges = PrivilegesType.STANDARD;
    }


    boolean arePersonsFriends(String id1, String id2, SqlDataProvider dataProvider) throws Exception {
        String sqlFriends = String.format(
                "SELECT * FROM person_is_friend_of WHERE PERSON_ID1 = %s AND PERSON_ID2 = %s;", id1, id2);

        ResultSet rsFriends = dataProvider.executeQuery(sqlFriends);
        return rsFriends.next();
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        String id1 = dataProvider.getCellValue(
                "person", "ID", "LOGIN", param("login1"), true);
        String id2 = dataProvider.getCellValue(
                "person", "ID", "LOGIN", param("login2"), true);


        if (arePersonsFriends(id1, id2, dataProvider)){
            throw new PersonsAreFriendsException();
        }


        String sql = String.format("INSERT INTO friendship_proposal VALUES (%s, %s);", id1, id2);
        dataProvider.executeUpdate(sql);
    }

    @Override
    public StatusCode invoke(SqlDataProvider dataProvider) {
        try{
            PrivilegesType userPrivileges = requiredPrivileges == PrivilegesType.NONE? PrivilegesType.NONE :
                    UserAuthorizer.authorize(param("login1"), param("password"), dataProvider);
            if (hasEnoughPrivileges(userPrivileges, requiredPrivileges)){
                procedureLogic(dataProvider);
                return StatusCode.OK;
            }
        } catch (Exception e){
        }
        return StatusCode.ERROR;
    }
}
