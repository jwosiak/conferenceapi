package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.util.Map;

/**
 * Created by jakub on 03.06.17.
 */
public class ProposalProcedure extends ConfProcedure {
    public ProposalProcedure(Map<String, String> parameters){
        super(parameters);
        requiredPrivileges = PrivilegesType.STANDARD;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        String sqlTalk =
                String.format("INSERT INTO talk (IDENTIFIER, TITLE, START) VALUES ('%s','%s',CAST('%s' AS DATE));",
                        param("talk"), param("title"), param("start_timestamp"));

        dataProvider.executeUpdate(sqlTalk);

        String talkIntId =
                dataProvider.getCellValue("talk", "ID", "IDENTIFIER", param("talk"), true);

        StringBuilder sqlPersonHasTalk = new StringBuilder();
        sqlPersonHasTalk
               .append("INSERT INTO person_has_talk ")
               .append(String.format("SELECT ID, %s ", talkIntId))
               .append(String.format("FROM person WHERE LOGIN = '%s'", param("login")));

        dataProvider.executeUpdate(sqlPersonHasTalk.toString());
    }
}
