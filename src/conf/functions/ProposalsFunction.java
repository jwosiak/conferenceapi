package conf.functions;

import conf.PrivilegesType;
import conf.SqlDataProvider;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jakub on 08.06.17.
 */
public class ProposalsFunction extends ConfFunction {
    public ProposalsFunction(Map<String, String> parameters) {
        super(parameters);
        requiredPrivileges = PrivilegesType.ADMIN;
    }

    @Override
    protected void procedureLogic(SqlDataProvider dataProvider) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql
                .append("SELECT * FROM talk JOIN person_has_talk ON (ID = TALK_ID) ")
                .append("JOIN person p ON (p.ID = PERSON_ID) WHERE ROOM_ID IS NULL ");

        result = dataProvider.executeQuery(sql.toString());
    }

    @Override
    protected HashMap<String, String> resultSetRowToTuple() throws SQLException {
        HashMap<String, String> tuple = new HashMap<>();

        tuple.put("talk", result.getString("IDENTIFIER"));
        tuple.put("start_timestamp", result.getString("START"));
        tuple.put("title", result.getString("TITLE"));
        tuple.put("speakerlogin", result.getString("LOGIN"));

        return tuple;
    }
}
