package conf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by jakub on 20.05.17.
 */
public class PsqlConnector {
    String dbName;
    String psqlUser;
    String psqlPassword;

    String connString;

    Connection connection;

    public PsqlConnector(String dbName, String psqlUser, String psqlPassword){
        this.dbName = dbName;
        this.psqlUser = psqlUser;
        this.psqlPassword = psqlPassword;

        connString = String.format("jdbc:postgresql://localhost:5432/%s", dbName);
    }


    public void connect() throws Exception {
        connection = DriverManager.getConnection(connString, psqlUser, psqlPassword);
     }

    public void disconnect() throws Exception {
        connection.close();
    }

    public Connection getConnection(){
        return connection;
    }
}
