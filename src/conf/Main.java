package conf;


import conf.functions.*;
import conf.io.FunctionsOutputWriter;
import conf.io.FunctionsReader;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    interface LazyProcedureConstructor{
        ConfProcedure construct(Map<String, String> parameters);
    }

    interface LazyFunctionConstructor{
        ConfFunction construct(Map<String, String> parameters);
    }

    public static void main(String args[]) {
/*        System.setErr(new PrintStream(new OutputStream() {
            public void write(int b) {
            }
        }));*/

	System.out.println("hello world");

        HashMap<String, LazyProcedureConstructor> procedures = new HashMap<>();

        procedures.put("open", params -> new OpenProcedure(params));
        procedures.put("organizer", params -> new OrganizerProcedure(params));
        procedures.put("event", params -> new EventProcedure(params));
        procedures.put("user", params -> new UserProcedure(params));
        procedures.put("talk", params -> new TalkProcedure(params));
        procedures.put("register_user_for_event", params -> new RegisterForEventProcedure(params));
        procedures.put("attendance", params -> new AttendanceProcedure(params));
        procedures.put("evaluation", params -> new EvaluationProcedure(params));
        procedures.put("reject", params -> new RejectProcedure(params));
        procedures.put("proposal", params -> new ProposalProcedure(params));
        procedures.put("friends", params -> new FriendsProcedure(params));


        HashMap<String, LazyFunctionConstructor> functions = new HashMap<>();

        functions.put("user_plan", params -> new UserPlanFunction(params));
        functions.put("day_plan", params -> new DayPlanFunction(params));
        functions.put("best_talks", params -> new BestTalksFunction(params));
        functions.put("most_popular_talks", params -> new MostPopularTalksFunction(params));
        functions.put("attended_talks", params -> new AttendedTalksFunction(params));
        functions.put("abandoned_talks", params -> new AbandonedTalksFunction(params));
        functions.put("recently_added_talks", params -> new RecentlyAddedTalksFunction(params));
        functions.put("rejected_talks", params -> new RejectedTalksFunction(params));
        functions.put("proposals", params -> new ProposalsFunction(params));
        functions.put("friends_talks", params -> new FriendsTalksFunction(params));
        functions.put("friends_events", params -> new FriendsEventsFunction(params));

        SqlDataProvider sqlDataProvider = new SqlDataProvider();

        FunctionsOutputWriter writer = new FunctionsOutputWriter();

        FunctionsReader reader = new FunctionsReader(System.in);

        while(reader.next()){
            LazyProcedureConstructor procCons = procedures.get(reader.getFunName());
            LazyFunctionConstructor funCons = functions.get(reader.getFunName());
            List<HashMap<String, String>> resultTuples = null;

            StatusCode code;

            if (procCons != null){
                ConfProcedure proc = procCons.construct(reader.getParameters());
                code = proc.invoke(sqlDataProvider);
            }
            else{
                if (funCons != null){
                    ConfFunction func = funCons.construct(reader.getParameters());
                    code = func.invoke(sqlDataProvider);
                    if (code == StatusCode.OK) {
                        try {
                            resultTuples = func.results();
                        } catch (Exception e) {
                            code = StatusCode.ERROR;
                        }
                    }
                }
                else{
                    code = StatusCode.NOT_IMPLEMENTED;
                }
            }
            System.out.println(writer.output(code, resultTuples));
        }

        try {
        } catch (Exception e) {
        }

    }
}
